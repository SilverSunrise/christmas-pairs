package com.christmaspairs.es

data class HolidaySymbol(var view: Int, var isSymbolFlipped: Boolean = false, var isPairConfirmed: Boolean = false)