package com.christmaspairs.es

import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_game.*


class GameActivity : AppCompatActivity() {


    private var locationOneSymbol: Int? = null

    private var fliped: Int = 0

    private lateinit var holidayCardSymbol: List<HolidaySymbol>

    private lateinit var buttonsImagesSymbol: List<ImageButton>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        restartGame()
        val buttonIconImages: MutableList<Int> = dataList()
        changeSymbol(buttonIconImages)
        changeViewSymbol()
    }

    private fun changeSymbol(buttonIconImages: MutableList<Int>) {
        buttonsImagesSymbol.forEachIndexed { location, symbols ->
            symbols.setOnClickListener {
                changeDataSymbol(location)
                changeViewSymbol()
            }
        }

        holidayCardSymbol = buttonsImagesSymbol.indices.map { number ->
            HolidaySymbol(buttonIconImages[number])
        }
    }

    private fun dataList(): MutableList<Int> {
        buttonsImagesSymbol = listOf(
            symbol_1,
            symbol_2,
            symbol_3,
            symbol_4,
            symbol_5,
            symbol_6,
            symbol_7,
            symbol_8,
            symbol_9,
            symbol_10,
            symbol_11,
            symbol_12,
            symbol_13,
            symbol_14,
            symbol_15,
            symbol_16
        )

        val buttonSymbolImages: MutableList<Int> = mutableListOf(
            R.drawable.santa_symbol1,
            R.drawable.santa_symbol2,
            R.drawable.santa_symbol3,
            R.drawable.santa_symbol4,
            R.drawable.santa_symbol5,
            R.drawable.santa_symbol6,
            R.drawable.santa_symbol7,
            R.drawable.santa_symbol8

        )
        buttonSymbolImages.addAll(buttonSymbolImages)
        buttonSymbolImages.shuffle()

        return buttonSymbolImages
    }

    private fun changeDataSymbol(symbolLocation: Int) {
        val symbol = holidayCardSymbol[symbolLocation]
        locationOneSymbol = if (locationOneSymbol == null) {
            rollBackChanges()
            symbolLocation
        } else {
            isPairConfirmed(locationOneSymbol!!, symbolLocation)
            null
        }
        symbol.isSymbolFlipped = !symbol.isSymbolFlipped
    }

    private fun restartGame() {
        btn_restartGame.setOnClickListener {
            val intent = intent
            finish()
            startActivity(intent)
        }
    }

    private fun changeViewSymbol() {
        holidayCardSymbol.forEachIndexed { number, symbol ->
            val button = buttonsImagesSymbol[number]
            if (symbol.isPairConfirmed) {
                button.alpha = 0.44f
            }
            button.setImageResource(if (symbol.isSymbolFlipped) symbol.view else R.drawable.default_card)
        }
    }

    private fun isPairConfirmed(positionFirst: Int, positionSecond: Int) {
        if (holidayCardSymbol[positionFirst].view == holidayCardSymbol[positionSecond].view) {
            holidayCardSymbol[positionFirst].isPairConfirmed = true
            holidayCardSymbol[positionSecond].isPairConfirmed = true
            fliped += 1
            if (fliped == 8) {
                iv_win.visibility = View.VISIBLE
                btn_restartGame.visibility = View.VISIBLE
            }
            tv_score.text = "Found Pair : $fliped"
        }
    }

    private fun rollBackChanges() {
        for (symbol in holidayCardSymbol) {
            if (!symbol.isPairConfirmed) {
                symbol.isSymbolFlipped = false
            }
        }
    }


}